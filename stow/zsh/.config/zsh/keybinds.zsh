key[Home]="^[[1~"
key[End]="^[[4~"
key[Insert]="^[[2~"
key[Backspace]="\b"
key[Delete]="^[[3~"
key[Up]="^[[A"
key[Down]="^[[B"
key[Left]="^[[D"
key[Right]="^[[C"
key[PageUp]="^[[5~"
key[PageDown]="^[[6~"
key[Shift-Tab]="^[[Z"

[[ -n "${key[BackSpace]}" ]] && bindkey "${key[BackSpace]}" backward-delete-char
[[ -n "${key[Home]}"      ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey "${key[End]}" end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n "${key[Delete]}"    ]] && bindkey "${key[Delete]}" delete-char
[[ -n "${key[Up]}"        ]] && bindkey "${key[Up]}" vi-up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey "${key[Down]}" vi-down-line-or-history
[[ -n "${key[PageUp]}"    ]] && bindkey "${key[PageUp]}" beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey "${key[PageDown]}" end-of-buffer-or-history

[[ -n "${key[BackSpace]}" ]] && bindkey -M vicmd "${key[BackSpace]}" backward-delete-char
[[ -n "${key[Home]}"      ]] && bindkey -M vicmd "${key[Home]}" beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -M vicmd "${key[End]}" end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -M vicmd "${key[Insert]}" overwrite-mode
[[ -n "${key[Delete]}"    ]] && bindkey -M vicmd "${key[Delete]}" delete-char
[[ -n "${key[Up]}"        ]] && bindkey -M vicmd "${key[Up]}" vi-up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -M vicmd "${key[Down]}" vi-down-line-or-history
[[ -n "${key[PageUp]}"    ]] && bindkey -M vicmd "${key[PageUp]}" beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -M vicmd "${key[PageDown]}" end-of-buffer-or-history

# Use vim keys in tab complete menu:
zmodload zsh/complist

bindkey -v "^?" backward-delete-char

# History
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey -M menuselect "h" vi-backward-char
bindkey -M menuselect "j" vi-down-line-or-history
bindkey -M menuselect "k" vi-up-line-or-history
bindkey -M menuselect "l" vi-forward-char

bindkey -M vicmd "K" up-line-or-beginning-search
bindkey -M vicmd "J" down-line-or-beginning-search
#
# Edit line in vim with ctrl-e:
autoload edit-command-line
zle -N edit-command-line
bindkey '^e' edit-command-line
