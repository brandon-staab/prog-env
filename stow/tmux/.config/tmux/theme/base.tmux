set-option -g history-limit 30000
set-option -g default-terminal "screen-256color"
set-option -ag terminal-overrides "$TERM:smcup@:rmcup@" # https://superuser.com/a/314620/401540
set-option -ag terminal-overrides ",$TERM:RGB"

set-option -g focus-events on

# Undercurl support
# http://evantravers.com/articles/2021/02/05/curly-underlines-in-kitty-tmux-neovim/#fn1
# To check try:
# printf '\e[4:3mUnderlined\n'
set-option -ag terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
set-option -ag terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colours - needs tmux-3.0

set-option -g display-panes-time 4000 # slightly longer pane indicators display time
set-option -g display-time 4000       # slightly longer status messages display time

# Highlight window when it has new activity
set-window-option -g monitor-activity on
set-option -g visual-activity off

# Only shrink window if a smaller terminal is actively viewing it
set-window-option -g aggressive-resize on

# Color the selection
set-option -g mode-style 'reverse'

# start window numbers at 1 to match keyboard order with tmux window order
set-window-option -g base-index 1
set-window-option -g pane-base-index 1

# renumber windows sequentially after closing any of them
set-window-option -g renumber-windows on
