# Copy/Paste binds
bind-key -T copy-mode    Enter             send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
bind-key -T copy-mode-vi Enter             send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
bind-key -T copy-mode    MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
