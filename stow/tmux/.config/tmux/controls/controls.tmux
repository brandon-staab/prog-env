set-option -g mouse on
set-window-option -g xterm-keys on # for vim
set-window-option -g mode-keys vi
bind-key "C-p" previous-window
bind-key "C-n" next-window

# Open new windows in current pane path
bind c new-window -c "#{pane_current_path}"

# splitting panes with | and -
bind \\ split-window -h -c "#{pane_current_path}"
bind | split-window -h -c "#{pane_current_path}"
bind _ split-window -v -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
bind \" split-window -v -c "#{pane_current_path}"
